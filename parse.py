import logging
import os
import pickle as pkl
# import torch
# from torch import nn
# from torch.utils.data import Dataset, DataLoader, TensorDataset
# import torch.nn.functional as F
from itertools import chain
import pandas as pd
import numpy as np
import holidays
from sklearn.model_selection import train_test_split

paris_station = "ILEDEFRA6"

logging.basicConfig(level=logging.INFO)

## square around paris :
X_NORMALISER = (2.15, 2.6)    # longitude
Y_NORMALISER = (48.61, 49.06) # latitude
TIME_NORMALISER     = (0., 1000.) ## 1000 = 98% of 'delta selection-presentation'
DISTANCE_NORMALISER = (0., 5000.) ## 5000 = 95% of 'OSRM estimated distance'
FILLNA_VAL = -1
IDX_COLNAME = 'emergency vehicle selection'

data_dir   = "./data/"
x_test_add = os.path.join(data_dir, "x_test_additional_file.csv")
x_test     = os.path.join(data_dir, "x_test.csv")
x_train_add= os.path.join(data_dir, "x_train_additional_file.csv")
x_train    = os.path.join(data_dir, "x_train.csv")
y_train    = os.path.join(data_dir, "y_train_u9upqBE.csv")
categories_path = "categories.pkl"
train_data_path = os.path.join(data_dir, 'train.pkl')
val_data_path   = os.path.join(data_dir, 'val.pkl')
test_data_path  = os.path.join(data_dir, 'test.pkl')
train_dataset_path = os.path.join(data_dir, 'train.pth')
test_dataset_path  = os.path.join(data_dir, 'test.pth')


## Temperature :
################################################################################

# class TempDf:
temp_df = None
def load_temperatures():
    global temp_df
    # file generated with get_paris_weather.py :
    df = pd.read_csv('ILEDEFRA6_weather.csv')
    print(df.dtypes)
    # set time as index :
    df = df.set_index(pd.DatetimeIndex(df['Time']))
    # remove duplicates and sort :
    df = df.loc[~df.index.duplicated(keep='first')].sort_index()
    # create dataset as mean over hour :
    df_h = df.groupby(pd.Grouper(freq='H')).mean().dropna(how='all')
    # create dataset as mean over day :
    df_d = df.groupby(pd.Grouper(freq='d')).mean().dropna(how='all')
    temp_df = {'m':df, 'h':df_h, 'd':df_d}
    logging.info("temperature dataset loaded.")
#
#     def get_paris_weather(self, weather_type, grouper):
#         g = grouper.lower()
#         if self.temp_df is None:
#             self.load_temperatures()
#         def fun(ts):
#             try:
#                 return self.temp_df[g].iloc[self.temp_df[g].index.get_loc(ts, method='ffill')][weather_type]
#             except KeyError:
#                 return self.temp_df[g].iloc[self.temp_df[g].index.get_loc(ts, method='bfill')][weather_type]
#         return fun
# temp_df = TempDf()

################################################################################


class FirePandas():
    def __init__(self):
        self.categories_dict = None
        self.idx_df = None
        self.df   = None

    def train_test_split(self, test_size=.2, random_state=None):
        train, test = train_test_split(self.df, test_size=test_size, random_state=random_state)
        train_data = FirePandas()
        test_data  = FirePandas()
        train_data.df = train
        test_data.df  = test
        train_data.categories_dict = self.categories_dict
        test_data.categories_dict  = self.categories_dict
        return train_data, test_data

    def set_idx_df(self, df):
        k = 'emergency vehicle selection'
        #df.set_index(k)
        if self.idx_df is None:
            self.idx_df = df[k]
        elif not np.all(self.idx_df==df[k]):
            raise ValueError

    DUMMIES_SEP = ' '
    @staticmethod
    def to_str(x):
        if isinstance(x, str):
            return x
        return str(float(x))
    def get_dummies_names(self, k):
        cat = list(map(FirePandas.to_str, self.categories_dict[k])) + ['nan']
        return [k+self.DUMMIES_SEP+x for x in cat]
    def add_dummies(self, df, k):
        if not isinstance(df[k].dtype, pd.CategoricalDtype):
            raise ValueError
        dummies_names = self.get_dummies_names(k)
        dummies = pd.get_dummies(df[k], prefix=k, prefix_sep=self.DUMMIES_SEP, dummy_na=True)
        assert list(dummies.columns) == list(dummies_names)
        for k_d in dummies_names:
            df[k_d] = dummies[k_d]
    def to_categories(self, df, key, add_dummies=True):
        df[key] = df[key].astype(pd.CategoricalDtype(self.categories_dict[key], ordered=True))
        self.add_dummies(df, key)
    #to_categories_int = lambda df, key, min_max: df.__setitem__(key, df[key].astype(pd.CategoricalDtype(list(range(min_max[0], min_max[1]+1)), ordered=True))  )#.cat.codes.astype('int64'))
    normalize = lambda df, key, min_max: df.__setitem__(key, ((df[key]-min_max[0]) / (min_max[1]-min_max[0])).astype('float32').fillna(FILLNA_VAL) )
    to_float  = lambda df, key:          df.__setitem__(key, df[key].astype('float32'))

    @classmethod
    def load_csv(cls, x_path, x_add_path, y_path=None, categories_path='categories.pkl'):
        """ if y_path is not None then the items will be tuples (x, y) oterwise only x """
        self = cls()
        logging.info("Loading categories...")
        self.categories_dict = self.load_categories(categories_path)
        logging.info("Loading x csv file...")
        self.df = self.read_x_csv(x_path)
        logging.info("Loading x_add csv file...")
        self.df = self.df.merge(self.read_x_add_csv(x_add_path))
        if y_path is not None:
            logging.info("Loading x_add csv file...")
            self.df = self.df.merge(self.read_y_csv(y_path))
        self.idx_df = None
        return self

    def get_sub_dataframe(self, features):
        features_with_dummies = list(chain(*[self.get_dummies_names(f) if isinstance(self.df[f].dtype, pd.CategoricalDtype) else [f] for f in features]))
        return self.df[features_with_dummies]

    def save_pickle(self, path):
        self.df.to_pickle(path, compression=None)
    @classmethod
    def load_pickle(cls, path, categories_path='categories.pkl'):
        self = cls()
        logging.info("Loading categories...")
        self.categories_dict = self.load_categories(categories_path)
        self.df = pd.read_pickle(path)
        return self

    def get_tensors(self, x_features, y_features=None):
        logging.info("Creating tensors...")
        import torch
        t_idx = torch.tensor(self.df.index.values, dtype=torch.int64)
        t_x   = torch.tensor(self.get_sub_dataframe(x_features).values.astype('float32'))
        if y_features is None:
            return t_idx, t_x
        t_y   = torch.tensor(self.get_sub_dataframe(y_features).values.astype('float32'))
        return t_idx, t_x, t_y
    def get_Dataset(self, x_features, y_features=None):
        from torch.utils.data import TensorDataset
        return TensorDataset(*self.get_tensors(x_features, y_features=y_features))

    def get_DMatrix(self, x_features, y_features=None):
        from xgboost import DMatrix
        logging.info("Creating DMatrix...")
        if y_features is None:
            return DMatrix(self.get_sub_dataframe(x_features))
        return DMatrix(self.get_sub_dataframe(x_features), label=self.get_sub_dataframe(y_features))

    def get_numpy(self, x_features, y_features=None):
        logging.info("Creating numpy arrays...")
        idx = self.df.index.values.astype('int64')
        x = self.get_sub_dataframe(x_features).values.astype('float32')
        if y_features is None:
            return idx, x
        y = self.get_sub_dataframe(y_features).values.astype('float32')
        return idx, x, y


      ################
     ###  Read x: ###
    ################
    def read_x_csv(self, x_path):
        df = pd.read_csv(x_path)

        # 'intervention'
        self.set_idx_df(df)

        # entier dans [1,9]
        k = 'alert reason category'
        self.to_categories(df, k)
        #df[k] = df[k] - 1

        k = 'alert reason'
        self.to_categories(df, k)

        # entier dans {0,1}
        k = 'intervention on public roads'
        FirePandas.to_float(df, k)
        #FirePandas.to_float(df, k)

        k = 'floor'
        FirePandas.to_float(df, k)
        FirePandas.normalize(df, k, (0., 10.))

        ## ?? distance?
        k = 'location of the event'
        FirePandas.normalize(df, k, (100., 325.))

        k = 'longitude intervention'
        FirePandas.normalize(df, k, X_NORMALISER)
        k = 'latitude intervention'
        FirePandas.normalize(df, k, Y_NORMALISER)

        k = 'emergency vehicle'
        self.to_categories(df, k)
        k = 'emergency vehicle type'
        self.to_categories(df, k)
        k = 'rescue center'
        self.to_categories(df, k)

        k_time = 'selection time'
        df[k_time] = pd.DatetimeIndex(df[k_time])

        k = k_time+' month'
        df[k] = df[k_time].map(lambda x:x.month)
        FirePandas.to_float(df, k)
        FirePandas.normalize(df, k, (1., 13.))

        k = k_time+' day'
        df[k] = df[k_time].map(lambda x:x.day)
        FirePandas.to_float(df, k)
        FirePandas.normalize(df, k, (1., 32.))

        k = k_time+' hour'
        df[k] = df[k_time].map(lambda x:x.hour)
        FirePandas.to_float(df, k)
        FirePandas.normalize(df, k, (0., 24.))

        k = k_time+' minute'
        df[k] = df[k_time].map(lambda x:x.minute)
        FirePandas.to_float(df, k)
        FirePandas.normalize(df, k, (0., 60.))

        k = k_time+' day of week'
        df[k] = df[k_time].map(lambda x:x.dayofweek)
        FirePandas.to_float(df, k)
        FirePandas.normalize(df, k, (0., 6.))

        fr_holidays = holidays.FRA()
        k = k_time+' is holiday'
        df[k] = df[k_time].map(lambda x: x in fr_holidays)
        FirePandas.to_float(df, k)

        if temp_df is None:
            load_temperatures()
        for grouper in ['M', 'H', 'd']:
            df_time_ordered = df.reset_index().sort_values(k_time)
            df_weather = pd.merge_asof(df_time_ordered, temp_df[grouper.lower()], right_index=True, left_on=k_time ,suffixes=('_x', ''),direction='backward').sort_index()
            for weather_type in ['dailyrainMM', 'TemperatureC']:
                k = 'weather {} {}'.format(weather_type, grouper)
                df[k] = df_weather[weather_type]
                FirePandas.to_float(df, k)
                if weather_type=='dailyrainMM':
                    FirePandas.normalize(df, k, (0, 20))
                elif weather_type=='TemperatureC':
                    FirePandas.normalize(df, k, (-7., 37.))


        # useless :
        # k = 'date key sélection'
        # k = 'time key sélection'

        ## int in {rentré, dispo}
        k = 'status preceding selection'
        self.to_categories(df, k)

        # in seconds
        k = 'delta status preceding selection-selection'
        FirePandas.normalize(df, k, TIME_NORMALISER)

        k = 'departed from its rescue center'
        FirePandas.to_float(df, k)

        k = 'longitude before departure'
        FirePandas.normalize(df, k, X_NORMALISER)
        k = 'latitude before departure'
        FirePandas.normalize(df, k, Y_NORMALISER)

        ## NAN inside !!
        k = 'delta position gps previous departure-departure'
        FirePandas.normalize(df, k, (0., 40.))

        ## format : "x0,y0;x1,y1;...;xn,yn"
        #k = 'GPS tracks departure-presentation'
        ## format : "t0;t1;...;tn"
        k_dt = 'GPS tracks datetime departure-presentation'
        df[k_dt] = df[k_dt].map(lambda l:pd.DatetimeIndex(l.split(';')) if l==l else l )
        k = k_dt + ' selection-first'
        df[k] = (df[k_dt].map(lambda l:l[ 0] if not isinstance(l,float) else l) - df['selection time']).map(lambda x:x.total_seconds())
        FirePandas.normalize(df, k, TIME_NORMALISER)
        k = k_dt + ' selection-last'
        df[k] = (df[k_dt].map(lambda l:l[-1] if not isinstance(l,float) else l) - df['selection time']).map(lambda x:x.total_seconds())
        FirePandas.normalize(df, k, TIME_NORMALISER)
        k = k_dt + ' first-last'
        df[k] = df[k_dt+' selection-last'] - df[k_dt+' selection-first']

        # ugly format
        #k = 'OSRM response'

        k = 'OSRM estimated distance'
        FirePandas.normalize(df, k, DISTANCE_NORMALISER)

        k = 'OSRM estimated duration'
        FirePandas.normalize(df, k, TIME_NORMALISER)

        return df

        # print(df.dtypes,'\n\n')
        # self.tensors = {}
        # all_features = list(x_features)+list(y_features if y_features is not None else [])
        # for k in all_features:
        #     if k in df.columns:
        #         self.tensors[k] = torch.tensor(df[k])

      ####################
     ###  Read x_add: ###
    ####################
    def read_x_add_csv(self, x_add_path):
        df = pd.read_csv(x_add_path)

        self.set_idx_df(df)

        # 'emergency vehicle selection'
        # 'OSRM estimate from last observed GPS position'

        k = 'OSRM estimated distance from last observed GPS position'
        FirePandas.normalize(df, k, DISTANCE_NORMALISER)

        k = 'OSRM estimated duration from last observed GPS position'
        FirePandas.normalize(df, k, TIME_NORMALISER)

        k = 'time elapsed between selection and last observed GPS position'
        FirePandas.normalize(df, k, TIME_NORMALISER)

        k = 'updated OSRM estimated duration'
        FirePandas.normalize(df, k, TIME_NORMALISER)

        return df


      ################
     ###  Read y: ###
    ################
    def read_y_csv(self, y_path):
        df = pd.read_csv(y_path)

        self.set_idx_df(df)

        #'emergency vehicle selection'
        k = 'delta selection-departure'
        FirePandas.normalize(df, k, TIME_NORMALISER)

        k = 'delta departure-presentation'
        FirePandas.normalize(df, k, TIME_NORMALISER)

        k = 'delta selection-presentation'
        FirePandas.normalize(df, k, TIME_NORMALISER)

        return df


    def get_categories(self, x):
        try:
            return list(self.categories[x])
        except TypeError:
            print("no categories for '{}'".format(x))
            raise ValueError

    @staticmethod
    def make_categories(csv_path, max_cat=1000):
        df = pd.read_csv(csv_path)
        keys = list(df.columns)
        cats = []
        for k in keys:
            v = df[k].sort_values().unique()
            cats.append(list(v) if len(v)<max_cat else None)
        return dict(zip(keys, cats))
    @staticmethod
    def save_categories(dict, categories_path='categories.pkl'):
        with open(categories_path, 'wb') as f:
            pkl.dump(dict, f)
    @staticmethod
    def load_categories(categories_path='categories.pkl'):
        with open(categories_path, 'rb') as f:
            dict = pkl.load(f)
        return dict



if __name__=="__main__":

    # categories_dict = {}
    # categories_dict.update(FireDataset.make_categories(x_train))
    # categories_dict.update(FireDataset.make_categories(x_train_add))
    # categories_dict.update(FireDataset.make_categories(y_train))
    # FireDataset.save_categories(categories_dict, categories_path=categories_path)
    # categories_dict = FireDataset.load_categories(categories_path)

    x_features = [
        'alert reason category', 'alert reason',
        'intervention on public roads', 'floor',
        'location of the event', 'longitude intervention', 'latitude intervention',
        'emergency vehicle', 'emergency vehicle type', 'rescue center',
        #'selection time',
        #'selection time',
        'status preceding selection', 'delta status preceding selection-selection',
        'departed from its rescue center', 'longitude before departure',
        'latitude before departure',
        'delta position gps previous departure-departure',
        #'GPS tracks departure-presentation',
        #'GPS tracks datetime departure-presentation',
        'GPS tracks datetime departure-presentation selection-first',
        'GPS tracks datetime departure-presentation selection-last',
        'GPS tracks datetime departure-presentation first-last',
        'OSRM estimated distance', 'OSRM estimated duration',
        'selection time month',
        'selection time day',
        'selection time hour',
        'selection time minute',
        'selection time day of week',
        'selection time is holiday',
        'OSRM estimated distance from last observed GPS position',
        'OSRM estimated duration from last observed GPS position',
        'time elapsed between selection and last observed GPS position',
        'updated OSRM estimated duration']
    y_features = [
        'delta selection-departure',
        'delta departure-presentation',
        'delta selection-presentation'
    ]
    y0_feature = [
            'delta selection-departure',
        ]
    y1_feature = [
            'delta departure-presentation',
        ]

    ## Create Datasets :
    train_data = FirePandas.load_csv(x_path=x_train, x_add_path=x_train_add, y_path=y_train, categories_path=categories_path)
    train_data, val_data = train_data.train_test_split(test_size=.3, random_state=12)
    train_data.save_pickle(train_data_path)
    val_data  .save_pickle(  val_data_path)
    val_data   = None
    train_data = None
    test_data = FirePandas.load_csv(x_path=x_test, x_add_path=x_test_add, categories_path=categories_path)
    test_data.save_pickle(test_data_path)
    print(test_data.df.info(verbose=True))
    test_data = None

    ## Load Datasets :
    # train_data = FirePandas.load_pickle(train_data_path, categories_path=categories_path)
    # train = train_data.get_Dataset(x_features, y_features=y_features)
    # train = train_data.get_DMatrix(x_features, y_features=y0_feature)
    # train = train_data.get_DMatrix(x_features, y_features=y1_feature)
    # train = train_data.get_numpy(  x_features, y_features=y_features)
    #
    # val_data = FirePandas.load_pickle(val_data_path, categories_path=categories_path)
    # val = val_data.get_Dataset(x_features, y_features=y_features)
    # val = val_data.get_DMatrix(x_features, y_features=y0_feature)
    # val = val_data.get_DMatrix(x_features, y_features=y1_feature)
    # val = val_data.get_numpy(  x_features, y_features=y_features)
    #
    # test_data  = FirePandas.load_pickle( test_data_path, categories_path=categories_path)
    # test =  test_data.get_Dataset(x_features)
    # test =  test_data.get_DMatrix(x_features)
    # test =  test_data.get_numpy(  x_features)
