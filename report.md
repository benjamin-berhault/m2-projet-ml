
# Paris Fire Brigade data challenge
##### Pierre Guetschel
##### January 2020

*Les scores donnés sont ceux retournés par le serveur sur l'ensemble de test dans la colonne* ***Public score*** *de la section* ***Submissions***

---


### Préprocessing Naïf

Avant toute chose j'ai commencé par traiter les données de la manière suivante :
 - diviser toutes les **durées** par `1000`, car 98% des valeurs de `'delta selection-presentation'` sont inférieures à `1000`.
 - normaliser les **distances** par `5000` car 95% des `'OSRM estimated distance'` sont inférieures à `5000`.
 - normaliser les **coordonnées GPS** par `X_NORM=(2.15, 2.6); Y_NORM=(48.61, 49.06)`, qui corresponds à un carré centré sur Paris.
 - transformer les **catégories** (ex: `'emergency vehicle type'`, `'alert reason category'`...) en versions one hot.
 - extraire des **date-time** les mois, jour, heure et minute et les normaliser (par resp. `12`, `31`, `24` et `60`).
 - remplacer les **NaN** par `-1`.

Ce premier traitement ramène quasiment toutes les valeurs dans l'intervalle `[0,1]` et `-1` pour les données manquantes.

Ensuite j'ai sélectionné les features suivantes : `['alert reason category',
    'intervention on public roads', 'floor',
    'location of the event', 'longitude intervention', 'latitude intervention',
    'emergency vehicle type', 'rescue center',
    'status preceding selection', 'delta status preceding selection-selection',
    'departed from its rescue center', 'longitude before departure',
    'latitude before departure',
    'delta position gps previous departure-departure',
    'OSRM estimated distance', 'OSRM estimated duration',
    'selection time month',
    'selection time day',
    'selection time hour',
    'selection time minute',
    'OSRM estimated distance from last observed GPS position',
    'OSRM estimated duration from last observed GPS position',
    'time elapsed between selection and last observed GPS position',
    'updated OSRM estimated duration']`


### Premier Modèle

Le premier modèle testé a été un réseau de neurone basique à une seule couche cachée de largeur 100. Adam comme algorithme d'optimisation et une loss MSE :

```python
from pytorch import nn

net = nn.Sequential(
  nn.Linear(inDim, 100),
  nn.ReLU(),
  nn.Linear(100, 3)
)
optimizer = torch.optim.Adam(params=net.parameters())
loss_fun = nn.MSELoss()
```

J'ai ensuite mis à part 20% des données d'entraînement pour la validation puis effectué 40 epochs et gardé le modèle avec le meilleur score sur l'ensemble de validation. Etonnamment ce premier modèle très simple m'a permis d'obtenir un score de `0.261`.

En prenant successivement 5 parties différentes du dataset comme ensembles de validation j'ai pu obtenir, en suivant le même entraînement, 5 modèles différents. La moyenne de leur sorties a obtenue un score de `0.27468` et la médiane des sorties un score de `0.27475`.

Ces scores me plaçaient, au moment de leur publication, en première position du classement général. On remarque que le réseau a 3 sortie. On n'exploite donc même pas la dépendance entre les 3 sorties attendues. Ils s'explique selon moi par le fait que la grande simplicité du modèle agit comme une forme de régularisation, en plus de l'early-stopping. Toujours est-il qu'ils ont augmenté ma confiance en mon préprocessing naïf.


### Données additionnelles

#### Jours spéciaux
Avec le module python `holidays`, j'ai pu facilement récupérer les **jours fériés** français.

Avec la fonction `.dayOfWeek()` de pandas j'ai ajouté le **jour de la semaine**.

J'aurais souhaité, mais n'ai pas eu le temps, ajouter les jours de **vacances scolaires** ainsi que les jours de **grève**.

#### Méteo
Grâce au site www.wunderground.com j'ai récupérer des données météorologiques minute par minute sur toute l'année 2018. Notamment le nombre de mm de **pluie** par jour et la **température**, que j'ai normalisé respectivement par `20` et dans `[-7, 37]`.

Comme features j'ai ajouté les moyennes de ces valeurs par heure et moyennes (non glissantes de minuit à minuit) par jour.


### Modèle final

J'ai utilisé un algorithme de boosting pour mon modèle final avec la bibliothèque python  `xgboost` en gardant 20% des données pour la validation finale.

Pour chercher les paramètres (cf: https://xgboost.readthedocs.io/en/latest/parameter.html) optimaux j'ai effectué une "semi"-gridsearch avec validation croisée 5 plis sur les 80% de données restantes. Semi-gridsearch car j'ai d'abord cherché la combinaison optimale des paramètres `max_depth` et `min_child_weight`, puis en fixant ces paramètres à leur meilleur, j'ai cherché une bonne combinaison pour `subsample` et `colsample_bytree`. Et finalement j'ai cherché un bon `eta`, compromis entre le score et le temps de calcul.

Ce travail a été effectué 3 fois. Une pour prédire `"delta selection-departure"` et une pour prédire `"delta departure-presentation"` lorsque la donnée `"updated OSRM estimated duration"` est fournie et une lorsque elle n'est pas fournie. Les features retenues dans chacun des 3 cas sont celles qui subjectivement faisaient sens et n'ajoutaient pas trop de bruit. La combinaison de ces 3 modèles m'a prermis d'obtenir mon score final de `0.2859`.

 - features_select_depart:
 `['emergency vehicle type',
 'rescue center',
 'status preceding selection',
 'delta status preceding selection-selection',
 'departed from its rescue center',
 'GPS tracks datetime departure-presentation selection-first',
 'selection time hour',
 'time elapsed between selection and last observed GPS position']`
 - features_depart_pres_WITH_updated_osrm:
  `[
    'alert reason category',
    'intervention on public roads',
    'floor',
    'location of the event',
    'longitude intervention', 'latitude intervention',
    'emergency vehicle type',
    'longitude before departure', 'latitude before departure',
    'GPS tracks datetime departure-presentation selection-first',
    'GPS tracks datetime departure-presentation selection-last',
    'GPS tracks datetime departure-presentation first-last',
    'OSRM estimated distance',
    'selection time month',
    'selection time hour',
    'selection time day of week',
    'selection time is holiday',
    'OSRM estimated distance from last observed GPS position',
    'OSRM estimated duration from last observed GPS position',
    'time elapsed between selection and last observed GPS position',
    'updated OSRM estimated duration',
    'weather dailyrainMM H',
    'weather dailyrainMM d',
    ]`
 - features_depart_pres_WITHOUT_updated_osrm :
 `['alert reason category',
    'intervention on public roads',
    'floor',
    'location of the event',
    'longitude intervention', 'latitude intervention',
    'emergency vehicle type',
    'longitude before departure', 'latitude before departure',
    'GPS tracks datetime departure-presentation selection-first',
    'GPS tracks datetime departure-presentation selection-last',
    'GPS tracks datetime departure-presentation first-last',
    'OSRM estimated distance',
    'OSRM estimated duration',
    'selection time month',
    'selection time hour',
    'selection time day of week',
    'selection time is holiday',
    'weather dailyrainMM H',
    'weather dailyrainMM d',
    ]`
