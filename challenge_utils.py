import numpy as np

header = "emergency vehicle selection,delta selection-departure,delta departure-presentation,delta selection-presentation"

def save_submission(path, np_array):
    np.savetxt(path, np_array, delimiter=',', header=header, fmt=['%d']+['%f']*3)
    print("submission saved at '{}'".format(path))
